<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>kendo-dropdownlist_2023-2024</name>
   <tag></tag>
   <elementGuidId>9f3e89e0-9ede-4a6f-a201-81a19e8a7347</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>kendo-dropdownlist[name=&quot;Year&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//kendo-dropdownlist[@name='Year']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>kendo-dropdownlist</value>
      <webElementGuid>5c607a8b-bc39-4cc7-85c1-9f4835b6cc34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-widget k-dropdown k-header ng-pristine ng-valid ng-touched</value>
      <webElementGuid>889798af-3f59-4118-bba8-deda10ca5676</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Year</value>
      <webElementGuid>ca9ab3f2-1499-4dbf-a694-514122a88080</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>7202214e-3813-4085-a142-a8816942dedc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2023-2024</value>
      <webElementGuid>537d7504-cf2f-4cfb-b3c0-9c72420f8419</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ng-tns-0-2&quot;]/app-root[1]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;header_top_space_add&quot;]/app-fnotax[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container main-tax-pl-report-pg&quot;]/form[@class=&quot;ng-pristine ng-valid ng-touched&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-2 segment mt-4&quot;]/kendo-dropdownlist[@class=&quot;k-widget k-dropdown k-header ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>70682879-b79e-4fd5-ae84-ac539119f65e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//kendo-dropdownlist[@name='Year']</value>
      <webElementGuid>3337961f-6359-4f83-9dae-5b097dc8d49f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/kendo-dropdownlist</value>
      <webElementGuid>e2234353-9109-415a-95d4-e50146674ddf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//kendo-dropdownlist[@name = 'Year' and (text() = '2023-2024' or . = '2023-2024')]</value>
      <webElementGuid>75fdd338-feb1-47ac-89b2-b8bca90d9e86</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
