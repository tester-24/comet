<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_LOGIN WITH SPACE</name>
   <tag></tag>
   <elementGuidId>8ec15636-a27f-4c85-b5f4-097682bf90bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.login-space-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'LOGIN WITH SPACE')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e5490efc-cf46-4b24-b30a-8c588f326cce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-space-btn</value>
      <webElementGuid>89f17f67-8a8c-4c13-b0f2-f25e49362ed0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LOGIN WITH SPACE</value>
      <webElementGuid>b0f4f08e-3c69-4d7c-82ca-4b36d11e0d9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-startup[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;login-space&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row align-items-center row-padding&quot;]/div[@class=&quot;col-lg-6&quot;]/div[@class=&quot;login-content&quot;]/a[@class=&quot;login-space-btn&quot;]</value>
      <webElementGuid>82621d2d-6a94-4c1a-bab9-ea6201eeca60</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'LOGIN WITH SPACE')]</value>
      <webElementGuid>af4280a7-a885-4bd4-b9da-1999c1565bff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>f6996980-89e6-414c-b312-26af1f03b76a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'LOGIN WITH SPACE' or . = 'LOGIN WITH SPACE')]</value>
      <webElementGuid>938da5c9-8a13-48c8-b0ac-cb220b7915ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
