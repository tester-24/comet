<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>spanf95a654e-1087-4316-b0d4-707ec2ddf947</name>
   <tag></tag>
   <elementGuidId>f0eda497-d094-44f6-91de-b01a9e152729</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='f95a654e-1087-4316-b0d4-707ec2ddf947']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#f95a654e-1087-4316-b0d4-707ec2ddf947</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1f2be38b-592c-466a-bbb9-00e247bb958c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-input</value>
      <webElementGuid>85550577-efbc-48a5-bf40-3a262cffe5df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>09adc9ba-749b-4868-b80b-b22b1188494c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>f95a654e-1087-4316-b0d4-707ec2ddf947</value>
      <webElementGuid>f037ac66-1a65-472d-9ff3-e9b0fdde5883</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;f95a654e-1087-4316-b0d4-707ec2ddf947&quot;)</value>
      <webElementGuid>e2c6fb74-c8de-4318-941d-3f1cb0860f21</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='f95a654e-1087-4316-b0d4-707ec2ddf947']</value>
      <webElementGuid>b503fff0-f33d-48f4-96a9-72de2a3df583</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-c5a60d31-8388-41bf-a528-ea78e8137d4c']/span</value>
      <webElementGuid>c430e9b2-1e37-4272-b61c-ba750fe97189</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/kendo-dropdownlist/span/span</value>
      <webElementGuid>241d0f8c-7472-436c-8f0e-fd1333ec008c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'f95a654e-1087-4316-b0d4-707ec2ddf947']</value>
      <webElementGuid>fb978c47-2f83-4674-a116-a4e652709dba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
