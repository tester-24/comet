<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Category</name>
   <tag></tag>
   <elementGuidId>2889dd7d-7699-4991-9dd7-61c6bdbad470</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#bills</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='bills']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a8b44968-76dd-485f-bba2-a032ee54248e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input</value>
      <webElementGuid>35b31648-c7b2-4c02-a4fb-c7beacc7ed0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>bills</value>
      <webElementGuid>015f980f-42b2-4499-a2dd-fdbf6c7e9fd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Category</value>
      <webElementGuid>dea4de7c-f3ee-43c6-96d6-0b097ffb2e52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>8d5abc55-5bed-4df9-89a3-0952fc9d1c7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>bills</value>
      <webElementGuid>21a9a43b-ca6a-4e30-8529-45fdfd43d639</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bills&quot;)</value>
      <webElementGuid>e91f7ebc-5984-4ee0-a3be-fa5fe4e32691</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='bills']</value>
      <webElementGuid>d6ceb1db-a89e-4eac-91e8-d158b8b90c57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='summarydownload']/div/div/div[2]/form/fieldset/div/div/div[2]/input</value>
      <webElementGuid>c046ebab-e4fc-4886-99d6-3ddfb4b27325</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>9b6593d2-9b08-4b58-9a8b-9212ca524fbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'bills' and @name = 'Category' and @type = 'radio']</value>
      <webElementGuid>efb5e744-c66e-41d8-b3f0-b0677876d614</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
