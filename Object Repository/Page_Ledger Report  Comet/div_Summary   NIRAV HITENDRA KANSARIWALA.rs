<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Summary   NIRAV HITENDRA KANSARIWALA</name>
   <tag></tag>
   <elementGuidId>fc33fadc-b254-4349-8115-772d4e1f9373</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.px-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//app-ledger/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>5e0ff95f-07b7-4d9c-a009-6a6f81b1c0ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-12 px-0</value>
      <webElementGuid>cca050e3-99e7-4afc-b6fe-b339688a54fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Summary |  NIRAV HITENDRA KANSARIWALA </value>
      <webElementGuid>60f3d30d-a54e-4c6e-8bbd-5fff8e6d95f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ng-tns-0-2&quot;]/app-root[1]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;header_top_space_add&quot;]/app-ledger[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;row mb-3 mx-0&quot;]/div[@class=&quot;col-md-12 px-0&quot;]</value>
      <webElementGuid>9c5f5c80-8c8e-4294-b8f3-30c1f8d6a93f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-ledger/div/div/div/div</value>
      <webElementGuid>a61e5f0b-14d0-46b0-b42a-7bf7cf07b2a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Summary |  NIRAV HITENDRA KANSARIWALA ' or . = ' Summary |  NIRAV HITENDRA KANSARIWALA ')]</value>
      <webElementGuid>4d10da2c-e517-47f7-8b8e-75ec63562cd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
