<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit (2)</name>
   <tag></tag>
   <elementGuidId>8c82720a-9f32-4add-b15f-8a5ab301a5b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-block</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a98f8d16-f0bc-4b97-9346-df7511d4ec3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block</value>
      <webElementGuid>1849e054-071c-41de-adf5-149fad87a42c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Submit </value>
      <webElementGuid>b431b585-8e6e-45b8-b636-17dcc6b9d344</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;header_top_space_add&quot;]/app-portfolio[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container&quot;]/form[@class=&quot;ng-untouched ng-pristine ng-valid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-2 col-md-3 mt-3&quot;]/button[@class=&quot;btn btn-block&quot;]</value>
      <webElementGuid>d254fd54-5b20-4562-bc0e-fed91a1fa246</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>b927ca46-be86-4161-b273-b699eb25ce5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = ' Submit ' or . = ' Submit ')]</value>
      <webElementGuid>10183723-a876-4bd4-833b-b778dbf23ba0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
