<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Portfolio</name>
   <tag></tag>
   <elementGuidId>6ef6d8a2-722f-427a-a030-aa0b890b94b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarText']/ul/li/div/a[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>30fd3ad2-d722-4ca8-a81b-4a62a10cfb13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-item</value>
      <webElementGuid>6000434a-337b-42e5-9699-3057ed6bd018</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/portfolio</value>
      <webElementGuid>6dc29d41-0734-4b6e-899d-4b6b8cbc649b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Portfolio</value>
      <webElementGuid>b529e8c8-8282-4477-92b0-cb7e37aeafcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarText&quot;)/ul[@class=&quot;navbar-nav ml-auto menu_marg align-items-center&quot;]/li[@class=&quot;nav-item menu_hover dropdown show&quot;]/div[@class=&quot;dropdown-menu show&quot;]/a[@class=&quot;dropdown-item&quot;]</value>
      <webElementGuid>4a25f8d1-e9fc-4a92-8fc9-d90ce7ede79e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarText']/ul/li/div/a[2]</value>
      <webElementGuid>6ccfaa28-b061-473e-80eb-e81c3315a562</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Portfolio')]</value>
      <webElementGuid>cfe5dc50-32b5-40eb-a9be-6f08f93bf001</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/portfolio')]</value>
      <webElementGuid>88b116a8-38cb-4b1d-a9ca-075ab8749a16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>1d57c74f-6f95-4a95-adb4-4da14bf371df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/portfolio' and (text() = 'Portfolio' or . = 'Portfolio')]</value>
      <webElementGuid>9572e0ff-c7a0-47bb-b32f-e06bce4f2871</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
