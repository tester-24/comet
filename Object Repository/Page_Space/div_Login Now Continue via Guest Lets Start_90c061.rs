<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Login Now Continue via Guest Lets Start_90c061</name>
   <tag></tag>
   <elementGuidId>918599c5-2153-4cda-b6af-f4a21e4be4b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.btns.full_wrap</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form/div[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2d34081c-62bd-4606-99fa-1fa959c0f498</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btns full_wrap</value>
      <webElementGuid>140f4e86-8068-4d27-80e0-ae2c6a617d4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Login Now Continue via Guest Let’s Start your process</value>
      <webElementGuid>f4a05087-34d4-4ff3-9129-468b9f7c8f15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;fp-enabled&quot;]/body[@class=&quot;fp-viewing-0&quot;]/app-slider[1]/app-login[1]/div[@class=&quot;abs_form_wrap pink_section_form varela_reg&quot;]/div[@class=&quot;table_wrap&quot;]/div[@class=&quot;cell_wrap&quot;]/div[@class=&quot;gridContainer&quot;]/div[@class=&quot;form_wrap&quot;]/form[@class=&quot;ng-dirty ng-touched ng-valid&quot;]/div[@class=&quot;btns full_wrap&quot;]</value>
      <webElementGuid>13dcd82a-d50f-464c-8cfa-29da933361f1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[5]</value>
      <webElementGuid>d38b481d-46f5-4996-9d7d-0120569fff1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' Login Now Continue via Guest Let’s Start your process' or . = ' Login Now Continue via Guest Let’s Start your process')]</value>
      <webElementGuid>5f056efc-c756-45f7-9250-3c97015db285</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
