<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_2023-2024</name>
   <tag></tag>
   <elementGuidId>40cd1a78-10f3-4a37-8d59-bf8e5fb8fb10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#8e966dcc-85b3-4e35-893b-bdccdab2848a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='8e966dcc-85b3-4e35-893b-bdccdab2848a']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>efd3245d-e38e-40de-a251-7d55d515e757</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-input</value>
      <webElementGuid>f3931407-51a1-4b1a-9095-d1126cb826f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>c280a51c-938d-4c01-ad90-ad007168f675</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>8e966dcc-85b3-4e35-893b-bdccdab2848a</value>
      <webElementGuid>2b98d1d2-5b95-4838-8ca3-419f436aa054</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2023-2024</value>
      <webElementGuid>e419e958-7d67-45be-88ab-1dfec9de48a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;8e966dcc-85b3-4e35-893b-bdccdab2848a&quot;)</value>
      <webElementGuid>3e04b637-ad3f-4e86-af17-c2905ed779ef</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='8e966dcc-85b3-4e35-893b-bdccdab2848a']</value>
      <webElementGuid>22fac07c-892e-46b6-8528-6032ea0a0f41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-96410bd4-4a4d-47cb-b711-73551e857cb4']/span</value>
      <webElementGuid>1f6f94b0-bfe2-45af-86c2-005d3758824e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/kendo-dropdownlist/span/span</value>
      <webElementGuid>9e7d3a9f-463b-498e-9688-5fdb2a1c1196</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = '8e966dcc-85b3-4e35-893b-bdccdab2848a' and (text() = '2023-2024' or . = '2023-2024')]</value>
      <webElementGuid>8abf042c-9051-4e2f-b548-851b77275110</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
