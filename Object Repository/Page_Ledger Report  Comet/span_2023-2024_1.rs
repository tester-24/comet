<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_2023-2024_1</name>
   <tag></tag>
   <elementGuidId>82d675e9-ce70-4ee4-b051-021ae4727c47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ac44c615-9814-4339-9162-b265b52792c7</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='ac44c615-9814-4339-9162-b265b52792c7']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>681f43c6-7fd8-4c53-9e10-ce9b0702a21e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-input</value>
      <webElementGuid>3f7c40bc-81f4-48dd-bb47-3f2966f9a8d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>e094f642-f2f5-42c7-aa74-42ff04740009</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ac44c615-9814-4339-9162-b265b52792c7</value>
      <webElementGuid>6634e2b7-e2d0-471b-9833-132266314b6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2023-2024</value>
      <webElementGuid>d66c9e19-6c9b-4e5c-ba53-9db754deb3e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ac44c615-9814-4339-9162-b265b52792c7&quot;)</value>
      <webElementGuid>a8f6c27d-4bcb-4308-bc43-85d97c10042c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='ac44c615-9814-4339-9162-b265b52792c7']</value>
      <webElementGuid>1e08e5ee-d2f8-4ba8-9437-6a371125126b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-07c76387-ee29-4cce-a528-d934cb056e73']/span</value>
      <webElementGuid>7e80c91b-ccf3-42e5-8433-2608fc19c296</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/kendo-dropdownlist/span/span</value>
      <webElementGuid>9c4b8d2a-baf7-4b72-be3d-714bf04e12db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = 'ac44c615-9814-4339-9162-b265b52792c7' and (text() = '2023-2024' or . = '2023-2024')]</value>
      <webElementGuid>4d300893-5078-4354-9e0a-aae045415b15</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
