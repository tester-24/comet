<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_text_form ng-untouched ng-pristine ng_7a20de</name>
   <tag></tag>
   <elementGuidId>90acf385-0ce8-4a42-a2ae-e28795c0ad87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.text_form.ng-untouched.ng-pristine.ng-invalid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@type='text']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>909e2664-40ff-44db-acda-8e3db4134a44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>e272f7ff-0f3a-480e-970a-e3df2cca01ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>435cf2ee-c026-4d39-858d-541154c2fe63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>one-time-code</value>
      <webElementGuid>f3e19c43-4821-4556-9193-aebe76a258a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>5231ef42-12ef-4370-b946-4e8d42832ef5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text_form ng-untouched ng-pristine ng-invalid</value>
      <webElementGuid>145576b5-b98b-4eab-b724-ddb3cfbc71ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;fp-enabled&quot;]/body[@class=&quot;fp-viewing-0&quot;]/app-slider[1]/app-login[1]/div[@class=&quot;abs_form_wrap pink_section_form varela_reg&quot;]/div[@class=&quot;table_wrap&quot;]/div[@class=&quot;cell_wrap&quot;]/div[@class=&quot;gridContainer&quot;]/div[@class=&quot;form_wrap&quot;]/form[@class=&quot;ng-untouched ng-pristine ng-invalid&quot;]/div[@class=&quot;full_wrap&quot;]/input[@class=&quot;text_form ng-untouched ng-pristine ng-invalid&quot;]</value>
      <webElementGuid>fd9ae34f-7ed0-4a52-b208-5baf5c3b95ff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@type='text']</value>
      <webElementGuid>b4137816-c642-4557-8a1e-58488cd34da0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>ee7ac23d-7c6d-4675-b15f-04192150a31a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>d8d11ff7-a23e-4c6f-9f1e-d6fa35f0287b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
