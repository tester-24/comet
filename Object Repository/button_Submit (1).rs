<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit (1)</name>
   <tag></tag>
   <elementGuidId>0ef89d5d-b472-4e17-8478-805458eba6bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-block</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>297d4bf6-1a01-4dc9-b25e-02b848c90c2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block</value>
      <webElementGuid>8d54db33-aa31-4f23-b489-4a04c75ab07c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Submit </value>
      <webElementGuid>010ae512-76e4-47d2-b3e0-120a0d9dda8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;header_top_space_add&quot;]/app-portfolio[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container&quot;]/form[@class=&quot;ng-untouched ng-pristine ng-valid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-2 col-md-3 mt-3&quot;]/button[@class=&quot;btn btn-block&quot;]</value>
      <webElementGuid>26fed2c8-2c46-4cdd-88bd-616b5fecee23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>ac4c67c5-bcce-4e9a-9182-81c613e8816c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = ' Submit ' or . = ' Submit ')]</value>
      <webElementGuid>4b49a4ea-3788-4997-9bb6-3fc8002a3932</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
