<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_View</name>
   <tag></tag>
   <elementGuidId>8624c3bd-4ec7-4721-b802-9458e691a006</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-block</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>76ee1666-4c78-4252-8ac6-421bb2deec1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block</value>
      <webElementGuid>7aaee75e-9fee-44ca-9dbb-10008effd137</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>0d4fd946-eaca-4eb7-938e-843860707dbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> View </value>
      <webElementGuid>b7db4025-9785-4c38-b342-60acb4e67c13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ng-tns-0-2&quot;]/app-root[1]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;header_top_space_add&quot;]/app-ledger[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container&quot;]/form[@class=&quot;ng-valid ng-dirty ng-touched&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-2 col-md-3 mt-3&quot;]/button[@class=&quot;btn btn-block&quot;]</value>
      <webElementGuid>fcfd3c85-1b78-4629-a951-eea92ca5ba76</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>62c60d81-bef3-4c04-9294-c642c4c2d14d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button</value>
      <webElementGuid>ba25b20a-34da-4698-9d51-4b12c75c4caf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = ' View ' or . = ' View ')]</value>
      <webElementGuid>076406d9-631c-431d-95da-c5d9e937ac00</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
