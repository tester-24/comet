<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_My Holding</name>
   <tag></tag>
   <elementGuidId>f321ffac-b68d-4d22-968c-4e68ff038d79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='86118dcf-4289-458f-acf8-bfd0cc8c6767']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#86118dcf-4289-458f-acf8-bfd0cc8c6767</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e3945a8a-b546-4119-89fa-5f132591ffbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-input</value>
      <webElementGuid>21dca5ce-022f-4ea0-9c47-c81812b3f0ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>fc46f2e2-6404-4fc7-9323-d2fabd74279b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>86118dcf-4289-458f-acf8-bfd0cc8c6767</value>
      <webElementGuid>65cd19e2-3f29-41c2-a5db-9f5e8a182b7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My Holding</value>
      <webElementGuid>070ace57-7de7-4dfa-a958-31c8f13aeed5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;86118dcf-4289-458f-acf8-bfd0cc8c6767&quot;)</value>
      <webElementGuid>79d0586d-d986-429d-ac38-46a931093677</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='86118dcf-4289-458f-acf8-bfd0cc8c6767']</value>
      <webElementGuid>c2976043-e2a3-4149-bc68-d99660debdfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-4c8606b2-9698-4380-9ef0-87c2c927d742']/span</value>
      <webElementGuid>5159a323-49ec-44be-adcd-b4e588f77c40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span</value>
      <webElementGuid>1d66a34a-1c5b-49a0-bb2a-735fb0759768</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = '86118dcf-4289-458f-acf8-bfd0cc8c6767' and (text() = 'My Holding' or . = 'My Holding')]</value>
      <webElementGuid>e7d2f0da-330e-4b99-8e83-ae1662871e2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='90279ae6-c27d-472c-b4ae-fd06a1bfebdb']</value>
      <webElementGuid>622f068c-fa56-473e-8db6-dc04af65be3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-b67e214a-ab61-4a6c-856a-8bdb296f2260']/span</value>
      <webElementGuid>ad1b8e1f-a40f-44c8-a612-bb222f2945f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = '90279ae6-c27d-472c-b4ae-fd06a1bfebdb' and (text() = 'My Holding' or . = 'My Holding')]</value>
      <webElementGuid>ccd5feeb-79a2-4448-9a7c-abe1e427f178</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@id='39ceb795-c249-48d8-88f5-3bde898e5889']</value>
      <webElementGuid>0825bad1-851e-4dcb-b4cb-824e9fe3036d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-a4160f39-aa84-4089-af8a-c666737f60a2']/span</value>
      <webElementGuid>b4ae0555-4687-4798-a445-e3dbf24c5d21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@id = '39ceb795-c249-48d8-88f5-3bde898e5889' and (text() = 'My Holding' or . = 'My Holding')]</value>
      <webElementGuid>7bb52aaf-1874-4a0b-b097-8640b617555a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
