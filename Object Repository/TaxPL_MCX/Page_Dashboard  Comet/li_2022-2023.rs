<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_2022-2023</name>
   <tag></tag>
   <elementGuidId>4ffd2d14-5385-4a10-9244-2b4f768d5b33</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#e2ffdc2a-53fd-45a6-8e4a-2a3136f23e95-2022</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//kendo-list/div/ul/li[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>3fb07dd4-0e5c-4c53-8cc8-a6381b96bf89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-item ng-star-inserted</value>
      <webElementGuid>1b45c9b5-7af1-4b96-8b79-1c74b7c523ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>3d87e21e-7447-491b-bd78-5817627b842d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>e2ffdc2a-53fd-45a6-8e4a-2a3136f23e95-2022</value>
      <webElementGuid>f6558041-35b2-43da-957c-586b5f981f28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>4b243fbd-8ee1-4a32-851e-e1f5cbf7e548</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>cb47800b-a8b8-4883-a0e6-6d38226f43fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2022-2023</value>
      <webElementGuid>d5155be3-906c-45ed-8c3f-414f649b51f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;e2ffdc2a-53fd-45a6-8e4a-2a3136f23e95-2022&quot;)</value>
      <webElementGuid>86fe9d4f-610d-44b2-bf45-4bedbdb940cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='e2ffdc2a-53fd-45a6-8e4a-2a3136f23e95-2022']</value>
      <webElementGuid>2ceb9646-502c-4c6b-abc1-c2a3f58b9174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='16a0b87c-f570-4f68-9a8a-464fbbf49e48']/li[2]</value>
      <webElementGuid>fa0ad0f2-1163-4ce5-9233-2ed4186d58dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//kendo-list/div/ul/li[2]</value>
      <webElementGuid>70c5d387-a5b4-429b-be30-811f186b04f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'e2ffdc2a-53fd-45a6-8e4a-2a3136f23e95-2022' and (text() = '2022-2023' or . = '2022-2023')]</value>
      <webElementGuid>e03ae0ab-cbe7-49b1-99b9-f4c16b3ac820</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='ea19625a-bf2b-464b-b78b-2f9a842e1138-2022']</value>
      <webElementGuid>02ada82c-7863-49e8-af56-d09da32db7da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='57cf39dd-af91-41ff-a9f7-8948f2783cf9']/li[2]</value>
      <webElementGuid>b4a00792-16fa-4a4a-81b8-7ad3ba7540a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'ea19625a-bf2b-464b-b78b-2f9a842e1138-2022' and (text() = '2022-2023' or . = '2022-2023')]</value>
      <webElementGuid>7d10fee5-446b-4c79-9bd7-0cabdd1a195c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
