<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Last 7 Days</name>
   <tag></tag>
   <elementGuidId>25d56b83-b90b-4ad4-a968-cc3f0de983ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='271f700e-42df-425e-9381-812c9a8a3e20-Last 7 Days']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>ece4c1a5-5a8c-4735-a2da-30d64d7c29c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-item k-state-focused k-state-selected ng-star-inserted</value>
      <webElementGuid>d78fd78c-8326-4a7a-b2e7-e565dc21809c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>5d11ccef-48cd-44d6-9e58-8cf67ad46bd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>271f700e-42df-425e-9381-812c9a8a3e20-Last 7 Days</value>
      <webElementGuid>f5c23180-778e-4d7e-94d5-be6c5caa273d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>af869611-a7cc-4699-936b-cbcf98c2ce3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>21884682-08fc-47d4-99db-98905c8fd963</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Last 7 Days</value>
      <webElementGuid>d509cbbe-a91d-42bd-84d6-8964f487ace9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;271f700e-42df-425e-9381-812c9a8a3e20-Last 7 Days&quot;)</value>
      <webElementGuid>1298ec94-fb05-4ac8-a2d0-a74da3af5985</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='271f700e-42df-425e-9381-812c9a8a3e20-Last 7 Days']</value>
      <webElementGuid>baa70375-d1ea-4388-a5fc-e70fbfeed5da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='42cb53d8-afee-4fa0-b4a3-43160b759e25']/li</value>
      <webElementGuid>9b396ede-5038-4160-a676-c90f6be7261a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//kendo-list/div/ul/li</value>
      <webElementGuid>d7580bfa-2096-48c5-bea1-d17712278702</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '271f700e-42df-425e-9381-812c9a8a3e20-Last 7 Days' and (text() = 'Last 7 Days' or . = 'Last 7 Days')]</value>
      <webElementGuid>590c5e3d-0952-40f9-9c00-bfbead7ce3b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='9c94f254-6012-4a84-a68b-b72e12994fbb-Last 7 Days']</value>
      <webElementGuid>5588e00a-c6f8-4180-918e-6c32c3ac6178</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='e2dfcce7-d5f5-45d3-a0ad-39faa5a97b4a']/li</value>
      <webElementGuid>ead3b946-5f54-4dcc-9acd-16b59e8ba927</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '9c94f254-6012-4a84-a68b-b72e12994fbb-Last 7 Days' and (text() = 'Last 7 Days' or . = 'Last 7 Days')]</value>
      <webElementGuid>cc93d69c-47f2-4969-89fa-d674816f7d48</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
