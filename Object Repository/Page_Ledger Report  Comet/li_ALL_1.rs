<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_ALL_1</name>
   <tag></tag>
   <elementGuidId>f79430a9-1e7c-411a-bdc7-fa274e369db1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#a4dc1065-2b36-4ea2-a4b9-8df813feed31-ALL</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='a4dc1065-2b36-4ea2-a4b9-8df813feed31-ALL']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>3e9d33f8-6171-458a-bf7c-f7cd90718785</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-item k-state-focused ng-star-inserted</value>
      <webElementGuid>4c82475e-a69f-4103-8761-a15cadb079c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>40ab79cc-6f3c-4d1e-b52b-e837a309f0d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>a4dc1065-2b36-4ea2-a4b9-8df813feed31-ALL</value>
      <webElementGuid>d28a5253-4d97-4ece-8ebd-e7f8807a9873</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>d0162916-46a3-4c67-9214-8c43a8bad09d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>d3964e84-7aca-46ae-ba7a-748e6fab7507</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ALL</value>
      <webElementGuid>de4a4d17-ff1b-4353-abe1-4d48399294e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;a4dc1065-2b36-4ea2-a4b9-8df813feed31-ALL&quot;)</value>
      <webElementGuid>8f58b8cf-65bd-4d0a-92d6-2776acb1df59</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='a4dc1065-2b36-4ea2-a4b9-8df813feed31-ALL']</value>
      <webElementGuid>1e65205f-36b7-484c-ae23-0825a7bdc601</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='437c2d12-aa69-4f75-8291-51e8831915d4']/li</value>
      <webElementGuid>6ada61e6-a8f8-47e9-924f-34021766ef4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//kendo-list/div/ul/li</value>
      <webElementGuid>2f77b33e-0a49-4df7-9a39-1bdefa366c94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'a4dc1065-2b36-4ea2-a4b9-8df813feed31-ALL' and (text() = 'ALL' or . = 'ALL')]</value>
      <webElementGuid>1be08128-df0b-461e-a7f6-812687a19091</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
