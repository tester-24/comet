<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_ALL_k-icon k-i-arrow-s</name>
   <tag></tag>
   <elementGuidId>e605fe87-c471-46e0-905f-e14fc66a02b7</elementGuidId>
   <imagePath>Screenshots/Targets/Page_Ledger Report  Comet/span_ALL_k-icon k-i-arrow-s.png</imagePath>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.k-icon.k-i-arrow-s</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='k-162c1e67-0788-4841-85f0-5912d3280dde']/span[2]/span</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Page_Ledger Report  Comet/span_ALL_k-icon k-i-arrow-s.png</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e508c98f-a791-4394-b8e4-f0409292d4f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-icon k-i-arrow-s</value>
      <webElementGuid>834b9264-add0-46a8-a82c-5871abb4f5a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>10e7b85c-3654-4fc7-a286-cd97c9687249</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;k-162c1e67-0788-4841-85f0-5912d3280dde&quot;)/span[@class=&quot;k-select&quot;]/span[@class=&quot;k-icon k-i-arrow-s&quot;]</value>
      <webElementGuid>2aa8ee92-8a8e-4d8e-8160-b6dc414515e7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-162c1e67-0788-4841-85f0-5912d3280dde']/span[2]/span</value>
      <webElementGuid>17c23608-d425-42f6-a89d-2f25c3aabf1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/span</value>
      <webElementGuid>dff26358-c8a6-4926-8d07-28f64489f5f6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
