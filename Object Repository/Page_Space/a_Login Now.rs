<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Login Now</name>
   <tag></tag>
   <elementGuidId>e1a42f73-a2ce-4e71-9b6d-5afa75dc8055</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.common_anchor.full.arrow.common_login_btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fb133045-6cbc-4904-bc86-4f3e27046aea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>90b708b7-a8c6-4b7f-b4ae-3418c1123879</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>41966acc-3454-47c8-b051-525a85353a27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>common_anchor full arrow common_login_btn</value>
      <webElementGuid>cb1d5506-1a1a-433f-8c8c-814bb8188527</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Login Now</value>
      <webElementGuid>cbc8ba05-15a3-4845-9d63-8079c9e86374</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;fp-enabled&quot;]/body[@class=&quot;fp-viewing-0&quot;]/app-slider[1]/app-login[1]/div[@class=&quot;abs_form_wrap pink_section_form varela_reg&quot;]/div[@class=&quot;table_wrap&quot;]/div[@class=&quot;cell_wrap&quot;]/div[@class=&quot;gridContainer&quot;]/div[@class=&quot;form_wrap&quot;]/form[@class=&quot;ng-dirty ng-touched ng-valid&quot;]/div[@class=&quot;btns full_wrap&quot;]/a[@class=&quot;common_anchor full arrow common_login_btn&quot;]</value>
      <webElementGuid>9614b180-a932-478c-99ab-f73cd7eadf98</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@type='submit']</value>
      <webElementGuid>6d8a4c09-9a78-466a-bfca-d2b94fa0ae92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Login Now')]</value>
      <webElementGuid>10e1c68c-c1c7-4cc5-a355-aea9d329dc05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a</value>
      <webElementGuid>d3909e88-ac76-4862-95a0-ff030e521597</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@type = 'submit' and (text() = ' Login Now' or . = ' Login Now')]</value>
      <webElementGuid>6655f811-0266-477f-a5d7-ced97d1acdfe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
