<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_DateWise</name>
   <tag></tag>
   <elementGuidId>36b5bb4b-8ea3-4edd-86c0-61710d5b06f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#e1cbdce9-7acb-4d0b-80a7-7e9fb086d877-DateWise</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='e1cbdce9-7acb-4d0b-80a7-7e9fb086d877-DateWise']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>8259ce78-972a-4467-bbde-cab94cee5942</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-item ng-star-inserted</value>
      <webElementGuid>65e6219c-4256-4dd2-aaf0-486ce4d72ece</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>433da8a8-4660-4d57-be84-c082459edc05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>e1cbdce9-7acb-4d0b-80a7-7e9fb086d877-DateWise</value>
      <webElementGuid>48ae2728-f792-4e24-a138-9222049b1fe1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>83be0a0f-d4bf-4792-89f1-354800ddea9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>4c51a16b-9a6f-4690-a823-3b013c707884</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DateWise</value>
      <webElementGuid>6de3ba96-49d3-4ccf-98fc-545cdf3f7749</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;e1cbdce9-7acb-4d0b-80a7-7e9fb086d877-DateWise&quot;)</value>
      <webElementGuid>22135fe0-b588-481a-8e9c-ed495b06ef1e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='e1cbdce9-7acb-4d0b-80a7-7e9fb086d877-DateWise']</value>
      <webElementGuid>0f1c26eb-cf1d-4e03-befb-16f766a2570b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='bb75e0ba-d7eb-437f-be13-640aed256d92']/li[2]</value>
      <webElementGuid>14984b8b-e496-484b-ae98-d55a6a81d896</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//kendo-list/div/ul/li[2]</value>
      <webElementGuid>30785b2b-ec73-410e-a271-6b6d409785d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'e1cbdce9-7acb-4d0b-80a7-7e9fb086d877-DateWise' and (text() = 'DateWise' or . = 'DateWise')]</value>
      <webElementGuid>a7086611-d842-4c82-ae82-c911cadd9ee2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='6b5be46e-ddbb-4679-86b6-fa5047dbddd3-DateWise']</value>
      <webElementGuid>302adf30-8dee-49d7-9311-71028fcec2a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='c2b34c75-b62d-4b52-a633-a7e03996e7f9']/li[2]</value>
      <webElementGuid>496cb3b1-a145-47e0-a269-7ec6876111e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '6b5be46e-ddbb-4679-86b6-fa5047dbddd3-DateWise' and (text() = 'DateWise' or . = 'DateWise')]</value>
      <webElementGuid>1a0e49a6-1e37-4ce1-b4f3-b59f3ce2a385</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='4d26c314-0f66-481c-bb67-95ca490d5ab6-DateWise']</value>
      <webElementGuid>9f26759f-210f-4f14-a032-f16d84466903</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='c2ecce5a-2753-418d-8c93-66db0f2f60ba']/li[2]</value>
      <webElementGuid>6553c700-93bb-446e-979b-6d9fa9f19eb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '4d26c314-0f66-481c-bb67-95ca490d5ab6-DateWise' and (text() = 'DateWise' or . = 'DateWise')]</value>
      <webElementGuid>5b82830d-3b2f-4069-857b-befbb326f2e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='094d61d6-bde6-4cda-a64f-e36829aa7397-DateWise']</value>
      <webElementGuid>6a40d671-6eac-4a10-9c6b-c45ca4928028</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='2ead86e9-46a7-4af8-a19d-c01ecf51421c']/li[2]</value>
      <webElementGuid>8bec06c2-51a0-4b67-a7c5-a89770874fba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '094d61d6-bde6-4cda-a64f-e36829aa7397-DateWise' and (text() = 'DateWise' or . = 'DateWise')]</value>
      <webElementGuid>93b02ecd-5cc8-4300-989d-715cfee163a1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
