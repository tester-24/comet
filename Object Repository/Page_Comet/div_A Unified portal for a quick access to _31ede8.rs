<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_A Unified portal for a quick access to _31ede8</name>
   <tag></tag>
   <elementGuidId>cf37318b-de47-4004-9a77-3dcd7b570a13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.login-space</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3b42fa59-6fe3-445b-94c7-8b86c40d08b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-space</value>
      <webElementGuid>cb028f54-69ba-47e0-884d-a4638057f469</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>A Unified portal for a quick access to all the functionalities provided by Jainam Group LOGIN WITH SPACE</value>
      <webElementGuid>bd22df7e-c47e-449b-9aea-b1838bb7095d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-startup[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;login-space&quot;]</value>
      <webElementGuid>f41e7fcf-0980-43d9-9cb4-2895b2be72cb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>6f23f6b4-2875-4276-a1fe-ea471bb5dadc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'A Unified portal for a quick access to all the functionalities provided by Jainam Group LOGIN WITH SPACE' or . = 'A Unified portal for a quick access to all the functionalities provided by Jainam Group LOGIN WITH SPACE')]</value>
      <webElementGuid>681f22c2-d00a-43ba-aab1-f53713e19cd3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
