<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img</name>
   <tag></tag>
   <elementGuidId>8f6066d9-37a6-4c90-8e1d-50b772aa37d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[5]/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>33336902-ceba-4f9d-8a29-49c6616955bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>../../../../assets/image/icon/attachment.png</value>
      <webElementGuid>17b8db16-ee4e-4c59-960b-35e53a2634b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
      <webElementGuid>0aff1c07-2bf4-4ac2-90cc-7e8e2b1d8537</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ng-tns-0-2&quot;]/app-root[1]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;header_top_space_add&quot;]/app-portfolio[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;mt-5 ng-star-inserted&quot;]/div[@class=&quot;mb-3&quot;]/form[@class=&quot;row justify-content-end align-items-center ng-untouched ng-pristine ng-valid&quot;]/div[@class=&quot;col-md-6 col-12 text-right&quot;]/button[@class=&quot;btn&quot;]/img[1]</value>
      <webElementGuid>323278b9-0289-4969-91bd-1030382725a6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[5]/img</value>
      <webElementGuid>2537acc7-443b-4266-84ec-96a0e82710f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = '../../../../assets/image/icon/attachment.png']</value>
      <webElementGuid>48e319cf-1608-416c-8f0d-4e3024b0ff53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]/img</value>
      <webElementGuid>501b1e4f-0a50-4cbd-8762-c8f5173928c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = '../../../../assets/image/icon/pdf.png']</value>
      <webElementGuid>afc48df7-244b-4882-893f-9866f0ea6665</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/img</value>
      <webElementGuid>1468fad0-635a-42c5-9718-5bda61699ae7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = '../../../../assets/image/icon/Email.png']</value>
      <webElementGuid>066e99be-9fa5-464c-bbed-ddf36f9e10f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[4]/img</value>
      <webElementGuid>890e2274-9fc8-4303-92c1-b7f6795dfc76</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
