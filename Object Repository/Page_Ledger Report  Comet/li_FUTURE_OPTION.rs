<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_FUTURE_OPTION</name>
   <tag></tag>
   <elementGuidId>7f1dab64-8cb0-4d0e-9903-b7b9a88e2758</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='ffd6e47a-08a9-4d1d-a428-82b0149af44a-FUTURE_OPTION']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ffd6e47a-08a9-4d1d-a428-82b0149af44a-FUTURE_OPTION</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>ccc4c44b-055d-42bc-bc41-0564828584b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-item ng-star-inserted</value>
      <webElementGuid>76b6e4ee-11ca-45d8-a490-db24bd80af07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>254aa513-c3ff-4a0e-a0a3-f4d45e78a54b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ffd6e47a-08a9-4d1d-a428-82b0149af44a-FUTURE_OPTION</value>
      <webElementGuid>3914612c-0e17-4343-9b0c-d677038f1f35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>8b615ebe-ac38-40c5-b0be-a2ac3f740a98</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>0e28f2f6-95d3-43c5-8a78-760bbf35d8b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>FUTURE_OPTION</value>
      <webElementGuid>581493ae-c39c-4b07-9617-33b82111a580</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ffd6e47a-08a9-4d1d-a428-82b0149af44a-FUTURE_OPTION&quot;)</value>
      <webElementGuid>574dd1ce-adc5-47f9-a6ba-6e8aa90cf07b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='ffd6e47a-08a9-4d1d-a428-82b0149af44a-FUTURE_OPTION']</value>
      <webElementGuid>f9c3b245-91e1-4683-aefe-359f172c8a04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='24d704c1-ebef-4876-a493-0af2ace17f37']/li[3]</value>
      <webElementGuid>3ebb707f-a865-4fe5-8df2-cd6ba3247d46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//kendo-list/div/ul/li[3]</value>
      <webElementGuid>a5d54201-6452-4674-be88-7900e67ef396</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'ffd6e47a-08a9-4d1d-a428-82b0149af44a-FUTURE_OPTION' and (text() = 'FUTURE_OPTION' or . = 'FUTURE_OPTION')]</value>
      <webElementGuid>49da6e7e-c1c0-4614-a2eb-7a8b97f0d820</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='ea226305-40a9-4fca-98a0-4b0ca75eb507-FUTURE_OPTION']</value>
      <webElementGuid>422ac605-db31-49e5-b726-2322e4959d81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='19387abd-8184-40f9-80f9-58c60451a394']/li[3]</value>
      <webElementGuid>d05c2798-eebd-4781-a647-35619c1d7166</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'ea226305-40a9-4fca-98a0-4b0ca75eb507-FUTURE_OPTION' and (text() = 'FUTURE_OPTION' or . = 'FUTURE_OPTION')]</value>
      <webElementGuid>2232fb88-4459-4ad9-ad87-4f4aba68adb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='6a454db8-647a-4e35-acf2-84811621b9ca-FUTURE_OPTION']</value>
      <webElementGuid>c0fda9dd-16f7-46ce-8c28-f8501ea436a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='a0b38013-a7c8-49a3-bf1b-acb00c31714e']/li[3]</value>
      <webElementGuid>06200a99-baa9-4576-9a61-960f5790563f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '6a454db8-647a-4e35-acf2-84811621b9ca-FUTURE_OPTION' and (text() = 'FUTURE_OPTION' or . = 'FUTURE_OPTION')]</value>
      <webElementGuid>cfe19fea-eee8-4396-a8f8-4be4830aad6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='e25b7640-2221-4c5d-a2dd-38942a112a50-FUTURE_OPTION']</value>
      <webElementGuid>5903482b-e6e2-497a-9347-0373143b07ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='caf6233f-e2cd-467e-bbe9-0fd41a29f3e7']/li[3]</value>
      <webElementGuid>90e7d180-8513-4b22-8a44-44176bdc0aae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'e25b7640-2221-4c5d-a2dd-38942a112a50-FUTURE_OPTION' and (text() = 'FUTURE_OPTION' or . = 'FUTURE_OPTION')]</value>
      <webElementGuid>79584ef4-7805-4995-bd61-b7d52d4b1921</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='599c44a8-08a1-4023-9386-0a833d2de405-FUTURE_OPTION']</value>
      <webElementGuid>fd9ba752-87e1-46bb-920a-0392bc93747c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='c0d5fd2c-f073-4693-8c6c-e7561dd2eded']/li[3]</value>
      <webElementGuid>1d371ee5-c711-4c5d-b502-ff959de15315</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '599c44a8-08a1-4023-9386-0a833d2de405-FUTURE_OPTION' and (text() = 'FUTURE_OPTION' or . = 'FUTURE_OPTION')]</value>
      <webElementGuid>067d6564-7be4-4713-b73b-88c9e068871d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='b682439b-867d-449d-aac0-3aa7fd4ec2a1-FUTURE_OPTION']</value>
      <webElementGuid>faca4730-e61b-4960-814f-747f668b44cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='8b2e0cf5-46a3-4066-8a0c-686eb2a28796']/li[3]</value>
      <webElementGuid>0ff5dd82-5387-48ff-a2bc-1f2dd626fc98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'b682439b-867d-449d-aac0-3aa7fd4ec2a1-FUTURE_OPTION' and (text() = 'FUTURE_OPTION' or . = 'FUTURE_OPTION')]</value>
      <webElementGuid>a9ef737f-2dce-40fa-a5ad-14f18b0ebc55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='260c4fdb-dcd0-4648-b947-4dd348c8fc85-FUTURE_OPTION']</value>
      <webElementGuid>8965c373-5f44-4626-aa37-14cd979c139b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='8ae648c3-2d50-4e47-a3e9-a07b098b54ad']/li[3]</value>
      <webElementGuid>a3113b2b-b680-434c-9c13-dcdadabe6f31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = '260c4fdb-dcd0-4648-b947-4dd348c8fc85-FUTURE_OPTION' and (text() = 'FUTURE_OPTION' or . = 'FUTURE_OPTION')]</value>
      <webElementGuid>41ac5c4c-28a5-4eb6-90bc-b4af0a8bb43f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
