<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>kendo-dropdownlist_2023-2024</name>
   <tag></tag>
   <elementGuidId>5a70f0cd-20c6-4a1d-a2a7-76ba857fe0e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//kendo-dropdownlist[@name='Year']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>kendo-dropdownlist[name=&quot;Year&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>kendo-dropdownlist</value>
      <webElementGuid>d9eded7c-a60a-4057-8f14-f7a2b38d5abf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-widget k-dropdown k-header ng-pristine ng-valid ng-touched</value>
      <webElementGuid>481604a0-80d2-4573-8405-0545db206198</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Year</value>
      <webElementGuid>e016c10a-186e-4d9a-bdc0-175b7926a10c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>a94bd02f-0160-4a57-be53-8ed9eeef1f1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2023-2024</value>
      <webElementGuid>66b6bc0c-946b-4b99-b120-30413e23dc79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ng-tns-0-2&quot;]/app-root[1]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;header_top_space_add&quot;]/app-tax[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;container main-tax-pl-report-pg&quot;]/form[@class=&quot;ng-pristine ng-valid ng-touched&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-2 segment mt-4&quot;]/kendo-dropdownlist[@class=&quot;k-widget k-dropdown k-header ng-pristine ng-valid ng-touched&quot;]</value>
      <webElementGuid>aedb299f-756e-4cde-8ea4-567eb4390aee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//kendo-dropdownlist[@name='Year']</value>
      <webElementGuid>5cef3a36-5138-4ca6-9255-cf15cabdb0a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/kendo-dropdownlist</value>
      <webElementGuid>24cc0a5d-ad84-473c-a3f6-e7bb8e39f3f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//kendo-dropdownlist[@name = 'Year' and (text() = '2023-2024' or . = '2023-2024')]</value>
      <webElementGuid>68182f54-0f6b-4281-950a-6c56d3bda5fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
