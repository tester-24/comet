<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Year_k-select</name>
   <tag></tag>
   <elementGuidId>46294701-589a-4eca-a3dd-51ca7d13c462</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#k-84e2484f-2925-480e-93a9-86711be878b2 > span.k-select</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/kendo-dropdownlist/span/span[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;k-84e2484f-2925-480e-93a9-86711be878b2&quot;)/span[@class=&quot;k-select&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>55759510-d6d8-4397-b4ad-6efdc5ba4a1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>k-select</value>
      <webElementGuid>2dae2a88-075c-4301-bd11-5de7bcd068ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>unselectable</name>
      <type>Main</type>
      <value>on</value>
      <webElementGuid>e1ed0e82-a3cf-4ade-8b5a-63c7db7d6e46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;k-84e2484f-2925-480e-93a9-86711be878b2&quot;)/span[@class=&quot;k-select&quot;]</value>
      <webElementGuid>b30d5ab5-144b-4a91-8e48-07a6d8df9d2e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='k-84e2484f-2925-480e-93a9-86711be878b2']/span[2]</value>
      <webElementGuid>960fb6d7-6dbf-46d1-b147-61218265dd2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/kendo-dropdownlist/span/span[2]</value>
      <webElementGuid>efd91bc7-5c6c-470a-94cf-0fed60e411ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
