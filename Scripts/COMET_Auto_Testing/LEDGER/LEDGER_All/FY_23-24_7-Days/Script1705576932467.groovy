import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://comet.jainam.in/#/startup')

WebUI.click(findTestObject('Object Repository/Page_Comet/a_LOGIN WITH SPACE'))

WebUI.setText(findTestObject('user_id/input_User Id_text_form ng-pristine ng-invalid ng-touched'), 'M3903')

WebUI.setText(findTestObject('user_pass/input_Password_LoginPassword'), 'Nirav@789')

WebUI.sendKeys(findTestObject('loginnow_button/a_Login Now'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Login_ID_01/input_text_form ng-untouched ng-pristine ng_7a20de'), '8')

WebUI.setText(findTestObject('Login_ID_02/input_Enter Pin_text_form ng-untouched ng-pristine ng-invalid'), '8')

WebUI.setText(findTestObject('Login_ID_03/input_Enter Pin_text_form ng-untouched ng-pristine ng-invalid'), '8')

WebUI.setText(findTestObject('Login_ID_04/input_Enter Pin_text_form ng-untouched ng-pristine ng-invalid'), '8')

WebUI.click(findTestObject('Object Repository/Page_Dashboard  Comet/a_Reports'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard  Comet/a_Ledger'))

WebUI.click(findTestObject('Object Repository/Page_Ledger Report  Comet/span_ALL'))

WebUI.click(findTestObject('Object Repository/Page_Ledger Report  Comet/li_ALL'))

WebUI.click(findTestObject('Object Repository/Page_Ledger Report  Comet/button_View'))

