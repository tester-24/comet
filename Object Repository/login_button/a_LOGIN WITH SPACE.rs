<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_LOGIN WITH SPACE</name>
   <tag></tag>
   <elementGuidId>b90c3bd0-ba2f-4e7f-a228-ab50d6ee3152</elementGuidId>
   <imagePath>Screenshots/Targets/Page_Comet/a_LOGIN WITH SPACE.png</imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Page_Comet/a_LOGIN WITH SPACE.png</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'LOGIN WITH SPACE')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.login-space-btn</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0a68ef20-8925-4c24-ac4f-7d4d9874fea5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-space-btn</value>
      <webElementGuid>fe3a951c-df2d-4478-ba1a-985102265af7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LOGIN WITH SPACE</value>
      <webElementGuid>d72a9f66-4f80-4deb-a5f7-026d4450f009</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-startup[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;login-space&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row align-items-center row-padding&quot;]/div[@class=&quot;col-lg-6&quot;]/div[@class=&quot;login-content&quot;]/a[@class=&quot;login-space-btn&quot;]</value>
      <webElementGuid>c89bca55-a987-4189-9ba1-cb94c2759d2e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'LOGIN WITH SPACE')]</value>
      <webElementGuid>9baa4293-2467-4a74-81bb-489e1929a787</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>e626f52d-2423-4bcd-a6a9-3d2af21150ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'LOGIN WITH SPACE' or . = 'LOGIN WITH SPACE')]</value>
      <webElementGuid>a22b7c30-6eef-48cd-b427-3347065f424e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
