<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_LOGIN WITH SPACE</name>
   <tag></tag>
   <elementGuidId>d5ab103f-7515-4985-8831-a8b8bf113b12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.login-space-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'LOGIN WITH SPACE')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>876ac93f-0fc8-46b6-9e4c-b0e17084154f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login-space-btn</value>
      <webElementGuid>9f774741-17bd-4ad5-b440-1442c25863ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LOGIN WITH SPACE</value>
      <webElementGuid>13949713-58f2-4d71-98b4-20d92f423ec5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-startup[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;login-space&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row align-items-center row-padding&quot;]/div[@class=&quot;col-lg-6&quot;]/div[@class=&quot;login-content&quot;]/a[@class=&quot;login-space-btn&quot;]</value>
      <webElementGuid>e0bc4c09-db9e-4b8e-9560-d4d31c1d856f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'LOGIN WITH SPACE')]</value>
      <webElementGuid>330d85f3-9bf9-46fc-8a8d-c5739c6a620d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>14016dea-4f4c-4734-adfc-293ab83fab3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'LOGIN WITH SPACE' or . = 'LOGIN WITH SPACE')]</value>
      <webElementGuid>50632d7d-255f-4b77-918e-2bb9f8c4128b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
