<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_input-group-text eye-hide-show</name>
   <tag></tag>
   <elementGuidId>8902bb5b-dcf3-4ef6-90c8-97cb85da0bbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.input-group-text.eye-hide-show</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[4]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>82ecbb30-a22f-4431-9c14-2a7a4f1b2746</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-group-text eye-hide-show</value>
      <webElementGuid>7f0a0c5a-843a-4da2-b806-85597ef0263f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;fp-enabled&quot;]/body[@class=&quot;fp-viewing-0&quot;]/app-slider[1]/app-login[1]/div[@class=&quot;abs_form_wrap pink_section_form varela_reg&quot;]/div[@class=&quot;table_wrap&quot;]/div[@class=&quot;cell_wrap&quot;]/div[@class=&quot;gridContainer&quot;]/div[@class=&quot;form_wrap&quot;]/form[@class=&quot;ng-dirty ng-touched ng-valid&quot;]/div[@class=&quot;full_wrap wrap_password_show&quot;]/span[@class=&quot;input-group-text eye-hide-show&quot;]</value>
      <webElementGuid>bb298f7a-0052-4560-af3b-c98d90e98ca3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/span</value>
      <webElementGuid>47b0b765-fb27-4ff4-a472-0dcb82a24fa7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
